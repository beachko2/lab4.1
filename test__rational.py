from unittest import TestCase
import Rational

class Test__sub__(TestCase):
    x1 = Rational.__sub__(0, 0)
    assert(x1 == 0)
    x1 = Rational.__sub__(1, 0)
    assert (x1 == 1)
    x1 = Rational.__sub__(0, 1)
    assert (x1 == -1)
    x1 = Rational.__sub__(5, 5)
    assert (x1 == 5)
    x1 = Rational.__sub__(-2, -3)
    assert (x1 == 1)
    x1 = Rational.__sub__(-1, 2)
    assert (x1 == -3)
    x1 = Rational.__sub__(-2, 1)
    assert (x1 == -3)
    x1 = Rational.__sub__(1, -2)
    assert (x1 == 3)
    x1 = Rational.__sub__(0, "String")
    assert TypeError
    x1 = Rational.__sub__("String", 0)
    assert TypeError

class Test__mul__(TestCase):
    x1 = Rational.__mul__(2, 3)
    assert (x1 == 6)
    x1 = Rational.__mul__(100, 300)
    assert (x1 == 30000)
    x1 = Rational.__mul__(-1, 8)
    assert (x1 == -8)
    x1 = Rational.__mul__(0, 10)
    assert (x1 == 0)
class Test__div__(TestCase):
    x1 = Rational.__div__(6, 3)
    assert(x1 == 2)

    x2 = Rational.__div__(10, 5)
    assert(x2 == 2)

    x3 = Rational.__div__(0, 2)
    assert ZeroDivisionError

    x3 = Rational.__div__(0, 2)
    assert ZeroDivisionError

class Test__str__(TestCase):
    s1 = Rational.__str__(25)
    assert(s1 == "25")
    s1 = Rational.__str__(0)
    assert (s1 == "0")
    s1 = Rational.__str__("a word")
    assert SyntaxError
    pass

class Test__float__(TestCase):
    x1 = Rational.__float__(0)
    assert(x1 == 0.0)

    x2 = Rational.__float__(2)
    assert(x2 == 2.0)

    x1 = Rational.__div__(1, 2)
    assert(x1 == 0.5)

    x1 = Rational.__div__(0, 2)
    assert(x1 == 0.0)

class Test__init__(TestCase):
    pass